﻿using Lab2.DAL;
using Lab2.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CenterToScreen();
            mostrar();
            mostrarRep();
        }

        public void mostrar()
        {
            dgvPlatillos.Rows.Clear();
            foreach (Platillo p in new PlatilloDAL().LeerXml())
            {
                if (p != null)
                {
                    btnCrearXML.Visible = false;
                }
                dgvPlatillos.Rows.Add(p, p.nombre, p.precio, p.calorias, p.descripcion);
            }
        }

        public void mostrarRep()
        {
            dgvReportes.Rows.Clear();
            foreach (Platillo p in new PlatilloDAL().Reporte())
            {
                dgvReportes.Rows.Add(p, p.nombre, p.precio, p.calorias, p.descripcion);
            }
        }

        public void limpiar()
        {
            txtNombre.ResetText();
            txtPrecio.ResetText();
            txtDescripcion.ResetText();
            txtCalorias.ResetText();
            
        }

        private void btnCrearXML_Click(object sender, EventArgs e)
        {
            PlatilloDAL xml = new PlatilloDAL();
            xml.CrearXML("Gestion_Platillos");
            mostrar();
            MessageBox.Show("Archivo XML creado", "Informacion");
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCalorias_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                Platillo p = new Platillo();
                p.nombre = txtNombre.Text.Trim();
                p.precio = double.Parse(txtPrecio.Text.Trim());
                p.calorias = int.Parse(txtCalorias.Text.Trim());
                p.descripcion = txtDescripcion.Text.Trim();

                new PlatilloDAL().AgregarDatos(p);
                mostrar();
                mostrarRep();
                limpiar();
            }
            catch (Exception)
            {

                MessageBox.Show("Intente nuevamente");
            }
        }

    }
}
