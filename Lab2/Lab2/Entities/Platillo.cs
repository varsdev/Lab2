﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Entities
{
    class Platillo
    {
        public string nombre { get; set; }
        public double precio { get; set; }
        public string descripcion { get; set; }
        public int calorias { get; set; }

        public override string ToString()
        {
            return nombre + " " + precio + " " + descripcion + " " + calorias;
        }

    }
}
