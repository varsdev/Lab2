﻿using Lab2.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Lab2.DAL
{
    class PlatilloDAL
    {
        XmlDocument doc;
        string rutaXml = "Datos.xml";

        public void CrearXML(string nodoRaiz)
        {
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            doc.Save(rutaXml);
        }

        public void AgregarDatos(Platillo p)
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNodeList listaPeliculas = doc.SelectNodes("Gestion_Platillos/platillo");
            foreach (XmlNode item in listaPeliculas)
            {
                if (item.FirstChild.InnerText == p.nombre)
                {
                    System.Windows.Forms.MessageBox.Show("Platillo registrado anteriormente");
                    return;

                }

            }
            XmlNode platillo = CrearPlatillo(p);
            XmlNode nodo = doc.DocumentElement;
            nodo.InsertAfter(platillo, nodo.LastChild);
            System.Windows.Forms.MessageBox.Show("Platillo registrado");
            doc.Save(rutaXml);
        }

        public XmlNode CrearPlatillo(Platillo p)
        {
            XmlNode Platillo = doc.CreateElement("platillo");

            XmlElement xnombre = doc.CreateElement("nombre");
            xnombre.InnerText = p.nombre;
            Platillo.AppendChild(xnombre);

            XmlElement xprecio = doc.CreateElement("precio");
            xprecio.InnerText = p.precio.ToString();
            Platillo.AppendChild(xprecio);

            XmlElement xdescripcion = doc.CreateElement("descripcion");
            xdescripcion.InnerText = p.descripcion;
            Platillo.AppendChild(xdescripcion);

            XmlElement xcalorias = doc.CreateElement("calorias");
            xcalorias.InnerText = p.calorias.ToString();
            Platillo.AppendChild(xcalorias);

            return Platillo;
        }

        public List<Platillo> LeerXml()
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNodeList listaPeliculas = doc.SelectNodes("Gestion_Platillos/platillo");
            List<Platillo> plat = new List<Platillo>();

            foreach (XmlNode item in listaPeliculas)
            {
                Platillo p = new Platillo();
                p.nombre = item.SelectSingleNode("nombre").InnerText;
                p.precio = double.Parse(item.SelectSingleNode("precio").InnerText);
                p.descripcion = item.SelectSingleNode("descripcion").InnerText;
                p.calorias = int.Parse(item.SelectSingleNode("calorias").InnerText);
                plat.Add(p);
            }
            return plat;
        }

        public List<Platillo> Reporte()
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNodeList listaPeliculas = doc.SelectNodes("Gestion_Platillos/platillo");
            List<Platillo> plat = new List<Platillo>();

            foreach (XmlNode item in listaPeliculas)
            {
                Platillo p = new Platillo();
                p.nombre = item.SelectSingleNode("nombre").InnerText;
                p.precio = double.Parse(item.SelectSingleNode("precio").InnerText);
                p.descripcion = item.SelectSingleNode("descripcion").InnerText;
                p.calorias = int.Parse(item.SelectSingleNode("calorias").InnerText);
                if (p.calorias >= 450 && p.calorias <= 700)
                {
                    if (p.precio <= 2500)
                    {
                        plat.Add(p);
                    }
                }             
            }
            return plat;
        }

        public void EliminarXml(string nombre)
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);

            XmlNodeList listaPeliculas = doc.SelectNodes("Gestion_Platillos/platillo");

            XmlNode borra = doc.DocumentElement;

            foreach (XmlNode item in listaPeliculas)
            {
                if (item.SelectSingleNode("nombre").InnerText == nombre)
                {
                    XmlNode borrar = item;
                    borra.RemoveChild(borrar);
                }
            }

            doc.Save(rutaXml);
        }

    }
}
